package com.example.demo.controller;

import com.example.demo.helper.ExcelHelper;
import com.example.demo.model.Image;
import com.example.demo.model.Product;
import com.example.demo.model.ProductModel;
import com.example.demo.repository.ImageRepository;
import com.example.demo.service.ExcelService;
import com.example.demo.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

//import org.springframework.mock.web.MockMultipartFile;

//import org.springframework.mock.web.MockMultipartFile;

@RestController
@RequestMapping("api")
@CrossOrigin(origins = "http://localhost:4200")
public class ProductController {
    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);
    @Autowired
    ProductService productService;

    @Autowired
    ImageRepository imageRepository;

    @Autowired
    ExcelService fileService;



    @GetMapping(value = "/product/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id) {
        Optional<Product> product = productService.findById(id);

        return new ResponseEntity<>(product, HttpStatus.OK);

    }

    @PostMapping("/product")
    @ResponseBody
    public ResponseEntity<Product> createProduct(@RequestBody Product product) {

        productService.save(product);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping(value = "/products", params = {"page"})
    public ResponseEntity<Page<Product>> getAllProduct(@RequestParam("page") int page) {
        Page products = productService.findAll(page);
        if (products.isEmpty()) {
            return null;
        }

        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/products")
    public ResponseEntity<List<Product>> getAllProduct() {
        List<Product> products = productService.findAll();
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @PutMapping("/product/{id}")
    public ResponseEntity<Optional<Product>> updateProduct(@RequestBody Product newProduct, @PathVariable Long id) {

        Optional<Product> p = productService.findById(id);
        if (!p.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        Product product = p.get();
        product.setProductName(newProduct.getProductName());
        product.setRemainedNumber(newProduct.getRemainedNumber());
        productService.save(product);
        return new ResponseEntity<>(p, HttpStatus.ACCEPTED);
    }


    @PreAuthorize("hasRole('ROLE_MODERATOR')")
    @DeleteMapping("/product/{id}")
    public ResponseEntity deleteProduct(@PathVariable Long id) {
        Optional<Product> p = productService.findById(id);
        if (!p.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        imageRepository.deleteImageByProduct(id);
        productService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/upload")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
        String message = "";

        if (ExcelHelper.hasExcelFormat(file)) {
            try {
                fileService.save(file);

                message = "Uploaded the file successfully: " + file.getOriginalFilename();
                return ResponseEntity.status(HttpStatus.OK).body("fsd");
            } catch (Exception e) {
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("oke");
            }
        }

        message = "Please upload an excel file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("new ResponseMessage(message)");
    }


    @GetMapping(value = "/download")
    public ResponseEntity<?> getFile(HttpServletResponse response) throws FileNotFoundException {
//log.info("start");
        String path = "C:\\Users\\mrv\\Desktop\\ex.xlsx";
        fileService.download(path);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/productpost")
    public ResponseEntity<?> createProduct(@ModelAttribute ProductModel product) {
        Product prod = new Product();
        prod.setProductName(product.getProductName());
        prod.setProductType(product.getProductType());
        prod.setCompanyName(product.getCompanyName());
        if(Objects.nonNull(product.getImages())){
            product.getImages().forEach(image -> {
                try {
                    Image img = new Image(image.getOriginalFilename(), image.getContentType(), image.getBytes(), prod);
                    prod.getImageSet().add(img);
                } catch (Exception e) {
                    logger.error("Convert model: ", e);
                }
             });
        }
        productService.save(prod);
        return new ResponseEntity<>(HttpStatus.OK);
    }

//    test oke
}