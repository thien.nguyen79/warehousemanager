package com.example.demo.model;

import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.List;

public class ProductModel implements Serializable {
    private static final long serialVersionUID = 765847637928439738L;

    private Long productId;
    private String productName;
    private Integer remainedNumber;
    private String companyName;
    private String productType;
    List<MultipartFile> images;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getRemainedNumber() {
        return remainedNumber;
    }

    public void setRemainedNumber(Integer remainedNumber) {
        this.remainedNumber = remainedNumber;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public List<MultipartFile> getImages() {
        return images;
    }

    public void setImages(List<MultipartFile> images) {
        this.images = images;
    }

    @Override
    public String toString() {
        return "ProductModel{" +
                "productId=" + productId +
                ", productName='" + productName + '\'' +
                ", remainedNumber=" + remainedNumber +
                ", companyName='" + companyName + '\'' +
                ", productType='" + productType + '\'' +
                '}';
    }
}
