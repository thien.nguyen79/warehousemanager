//package com.example.demo.model;
//
//import lombok.Getter;
//import lombok.Setter;
//
//import javax.persistence.Column;
//import javax.persistence.Embeddable;
//import java.io.Serializable;
//
//@Getter
//@Setter
//@Embeddable
//public class OrderDetailKey implements Serializable {
//    @Column(name = "order_id", nullable = false)
//    private Long oderId;
//
//    @Column(name = "product_id", nullable = false)
//    private Long productId;
//}