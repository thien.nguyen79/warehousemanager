package com.example.demo.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long productId;
    private String productName;
    private Integer remainedNumber;
    private String companyName;
    private String productType;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product",  cascade=CascadeType.ALL)
    private List<Image> imageSet = new ArrayList<>();

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public List<Image> getImageSet() {
        return imageSet;
    }

    public void setImageSet(List<Image> imageSet) {
        this.imageSet = imageSet;
    }

    public Product() {
    }

    public Integer getRemainedNumber() {
        return remainedNumber;
    }

    public void setRemainedNumber(Integer remainedNumber) {
        this.remainedNumber = remainedNumber;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }
}
