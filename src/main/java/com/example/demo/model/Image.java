package com.example.demo.model;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "images")
public class Image {
    @Id
    @Column(name = "id")

    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;

    @Column(name = "name")

    private String name;

    @Column(name = "type")

    private String type;

    @Column(name = "pic_byte", length = 1000)
    private byte[] picByte;

//    public Image(String name, String type, byte[] compressBytes) {
//        this.name= name;
//        this.type= type;
//        this.picByte= compressBytes;
//    }

    public Image(Long id, String name, String type, byte[] decompressBytes, Product product) {
        this.id= id;
        this.name= name;
        this.type= type;
        this.picByte= decompressBytes;
        this.product= product;
    }

    //image bytes can have large lengths so we specify a value

    //which is more than the default length for picByte column

    public byte[] getPicByte() {
        return picByte;
    }

    @ManyToOne
    @JoinColumn(name = "product_id") // thông qua khóa ngoại
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Product product;

    public Image(String originalFilename, String contentType, byte[] compressBytes, Product product1) {
        this.name= originalFilename;
        this.type= contentType;
        this.picByte= compressBytes;
        this.product= product1;
    }

    public void setPicByte(byte[] picByte) {
        this.picByte = picByte;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Image() {
    }
}

