package com.example.demo.request;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;

public class ProductRequest {

    private String productName;
    private Integer remainedNumber;
    private File image;

    public ProductRequest() {
    }

    public File getImage() {
        return image;
    }

    public void setImage(File image) {
        this.image = image;
    }

    public Integer getRemainedNumber() {
        return remainedNumber;
    }

    public void setRemainedNumber(Integer remainedNumber) {
        this.remainedNumber = remainedNumber;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
