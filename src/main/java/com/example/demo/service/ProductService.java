package com.example.demo.service;

import com.example.demo.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ProductService {

    //    Product product = new Product();
    void save(Product product);

    Page findAll(Integer page);

    List<Product> findAll();

    Optional<Product> findById(Long id);

    void deleteById(Long id);
}