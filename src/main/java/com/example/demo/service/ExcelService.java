package com.example.demo.service;

import com.example.demo.helper.ExcelHelper;
import com.example.demo.model.Product;
import com.example.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

@Service
public class ExcelService {
    @Autowired
    ProductRepository productRepository;

    public void save(MultipartFile file) {
        try {
            List<Product> tutorials = ExcelHelper.excelToProduct(file.getInputStream());
            productRepository.saveAll(tutorials);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }

    public List<Product> getAllTutorials() {
        return productRepository.findAll();
    }

    public void download(String path) {
        List<Product> products = productRepository.findAll();
        ExcelHelper.productToExcel(products, path);
    }
}
